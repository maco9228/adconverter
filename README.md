# ADConverter

## Overview

The task we would like you to complete is to create an app that makes a call for international currency data (JSON) and use it to create a currency converter app that converts international currencies into Australian Dollars.  Refer to Apple's Human Interface Guidelines or Android's material design as a resource to provide a good customer experience.

## GitLab Usage

Please be sure to clone this repository into your own project to complete the assessment.
Once the assessment is complete it should be shared back to madteam-devsupport with Developer access.
It is important you retain ownership and access control of your assessment.

## Requirements:

* The app must be written in Swift or Kotlin
* The app must be written using Xcode 9+ for iOS or Android Studio for Android.
* Use of suitable design patterns, and a strict separation of concerns when developing a mobile application
* Create a library containing all functionality that could be reused by 3rd party developers in any application that requires exchange rate calculations. 3rd parties using this library should not be tightly coupled to any specific technology and should be able to consume the library in any way they choose
* Code should be commented sufficiently to allow auto generation of library API documentation
* Use all the data the API provided
  * Asynchronous development principals when retrieving and displaying data originating from network calls
* UI interaction and data binding principals
  * Sound management of User Interface
  * The application should be able to be re-branded (colours, fonts, assets) - 2 distinct branded targets/variants should be included in the project
  * The app should be built with a universal UI.
* Correct use of the application life cycle, management of the UI thread
* Incremental Commits of code and proper use/understanding of gitflow
  * Quick Overview - <http://nvie.com/posts/a-successful-git-branching-model/>
  * In-depth Tutorial - <https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow>
* Unit tests/mocks to demonstrate the code is testable

## Getting started

ADConverter is currency converter which provides currency rate of around 30 currencies with respect to Australian Dollar.

## Installation

pod 'ADConverter'

## Screenshot

<img src="./screens/ADConverterDemo.png" width="350px" />

## Usage

To run the example project, clone the repo, and run 'pod install' from the ADConverterExample directory first.

```swift
import ADConverter
```

or as an outlet (supports auto layout)

```swift
@IBOutlet weak var currencyPickerView: CurrencyPickerView!
```

### Customizations

The aim is to allow the ADConverter to be as customizable as possible without making it overly complex and bloated.

#### Appearance

You can customize the look of the ADConverter by setting certain properties of `CurrencyPickerView`.

```swift
  currencyPickerView.titleFont = .systemFont(ofSize: 16.0)
  currencyPickerView.titleText = "This is title text"
  currencyPickerView.titleColor = .orange
        
  currencyPickerView.currencyFont = .systemFont(ofSize: 16.0)
  currencyPickerView.currencyColor = .orange
        
  currencyPickerView.countryFont = .systemFont(ofSize: 16.0)
  currencyPickerView.countryColor = .orange
        
  currencyPickerView.buyFont = .systemFont(ofSize: 16.0)
  currencyPickerView.buyColor = .orange
        
  currencyPickerView.sellFont = .systemFont(ofSize: 16.0)
  currencyPickerView.sellColor = .orange
```

You can even get the Conversion Rate for a particular currency and list of all currency rate.

```swift

func getINRRates() {
    CurrencyPickerView.getExchangeRate(currency: "INR") { rate, error in
            guard error == nil else {
              // Do semething in case of error
              return
            }
            
            // Currency object to be used with custom UI
            if let rate = rate {
              print(rate)
            } else {
              // Do semething in case of error
            }
        }
    }
    
    func getAllCurrencyRates() {
        CurrencyPickerView.getExchangeRate { response, error in
            guard error == nil else {
              // Do semething in case of error
              return
            }
            
            // Currency object to be used with custom UI
            if let response = response {
              print(response)
            } else {
              // Do semething in case of error
            }
      }
}

```

## Requirements

* iOS 9.0+
* Xcode 12.0

## Author

Manu Gupta, manugupta9228@gmail.com

## License

ADConverter is available under the MIT license. See the LICENSE file for more info.

