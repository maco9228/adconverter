import Foundation
import ADConverter

struct CurrencyRateViewModel {
    
    func getRate(for currency: String, completion: @escaping(Rate?, String?) -> ()) {
        CurrencyPickerView().getExchangeRate(currency: currency) { rate, error in
            guard error == nil else {
                // Do semething in case of error
                return
            }

            // Currency object to be used with custom UI
            // We can create model according to our custom requirement and pass it to controller
            if let rate = rate {
                print(rate)
            } else {
                // Do semething in case of error
            }
        }
    }
    
    func getAllCurrencyRates(completion: @escaping ([String: Product]?, String?) -> ()) {
        CurrencyPickerView().getExchangeRate { response, error in
            guard error == nil else {
                // Do semething in case of error
                return
            }

            // Currency object to be used with custom UI.We can create model according to our custom requirement and pass it to controller
            if let response = response {
                print(response)
            } else {
                // Do semething in case of error
            }
        }
    }
}
