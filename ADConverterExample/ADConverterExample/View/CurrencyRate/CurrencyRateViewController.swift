import UIKit
import ADConverter

class CurrencyRateViewController: UIViewController {

    @IBOutlet weak var currencyPickerView: CurrencyPickerView!
    lazy var viewModel: CurrencyRateViewModel = {
        return CurrencyRateViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getRate(for: "INR") { rate, errorString in
            // Handle according to the requirement
        }
        
        viewModel.getAllCurrencyRates() { rate, errorString in
            // Handle according to the requirement
        }
    }
}
