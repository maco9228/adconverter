//
//  CurrencyPickerView.swift
//  ADConverter
//
//  Created by manu.a.gupta on 20/03/22.
//

import UIKit

public class CurrencyPickerView: UIView {
    
    lazy private var contentView: UIView = makeContentView()
    lazy private var stackView: UIStackView = makeVerticalStackView()
    lazy private var titleLabel: UILabel = makeTitleLabel()
    lazy private var currencyTextField: UITextField = makeCurrencyTextField()
    lazy private var currencyDetailStackView: UIStackView = makeCurrencyStackView()
    lazy private var countryNameLabel: UILabel = makeCountryNameLabel()
    lazy private var currencyNameLabel: UILabel = makeCurrencyNameLabel()
    lazy private var rateDetailStackView: UIStackView = makeRateStackView()
    lazy private var buyPriceLabel: UILabel = makeBuyPriceLabel()
    lazy private var sellPriceLabel: UILabel = makeSellPriceLabel()
    lazy private var lastUpdatedLabel: UILabel = makeLastUpdatedLabel()
    
    private let pickerView = UIPickerView()
    private var pickerDataSource = ["BDT", "WST", "TOP", "CAD", "JPY", "TWD", "THB", "KRW", "SBD", "INR", "ZAR", "XPF", "PHP", "CNH", "BRL", "SGD", "HKD", "NOK", "USD", "MYR", "AED", "VND", "EUR", "ARS", "BND", "SEK", "PGK", "NZD", "SAR", "CLP", "FJD", "CNY", "VUV", "XAU", "PKR", "GBP", "IDR", "CHF", "DKK", "LKR"].sorted()
    private var selectedCurrency: String?
    private var textFieldHeightConstraint: NSLayoutConstraint?
     
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        setupUI()
    }
    
    private func setupUI() {
        addSubview(contentView)
        contentView.bindFrameToSuperviewBounds()
        
        stackView.addArrangedSubview(titleLabel)
        
        textFieldHeightConstraint = currencyTextField.heightAnchor.constraint(equalToConstant: 50)
        textFieldHeightConstraint?.isActive = true
        
        stackView.addArrangedSubview(currencyTextField)
        contentView.addSubview(stackView)
        
        countryNameLabel.isHidden = true
        currencyNameLabel.isHidden = true
        
        currencyDetailStackView.addArrangedSubview(countryNameLabel)
        currencyDetailStackView.addArrangedSubview(currencyNameLabel)
        stackView.addArrangedSubview(currencyDetailStackView)
        
        buyPriceLabel.isHidden = true
        sellPriceLabel.isHidden = true
        
        rateDetailStackView.addArrangedSubview(buyPriceLabel)
        rateDetailStackView.addArrangedSubview(sellPriceLabel)
        stackView.addArrangedSubview(rateDetailStackView)
        
        lastUpdatedLabel.isHidden = true
        stackView.addArrangedSubview(lastUpdatedLabel)

        stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 24).isActive = true
        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -24).isActive = true
        
        pickerView.delegate = self
        currencyTextField.inputView = pickerView
    }
    
    private func makeContentView() -> UIView {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeVerticalStackView() -> UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = .vertical
        stackView.spacing = 12
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    private func makeTitleLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.textColor = .black
        label.text = "Please select the Currency from picker to current conversion rate to Australian Dollar."
        return label
    }
    
    private func makeCurrencyTextField() -> UITextField {
        let textField = UITextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.setInputToobar(target: self, selector: #selector(tapDone))
        textField.layer.borderColor = UIColor.black.cgColor
        textField.layer.borderWidth = 1
        textField.setLeftPaddingPoints(10)
        return textField
    }
    
    private func makeCurrencyStackView() -> UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .leading
        stackView.spacing = 12
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    private func makeCountryNameLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 1
        label.textColor = .black
        return label
    }
    
    private func makeCurrencyNameLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 1
        label.textColor = .black
        return label
    }
    
    private func makeRateStackView() -> UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = .horizontal
        stackView.alignment = .leading
        stackView.distribution = .equalSpacing
        stackView.spacing = 12
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    private func makeBuyPriceLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 1
        label.textColor = .black
        return label
    }
    
    private func makeSellPriceLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 1
        label.textColor = .black
        return label
    }
    
    private func makeLastUpdatedLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 1
        label.textColor = .black
        return label
    }
    
    @objc private func tapDone() {
        if selectedCurrency == nil { selectedCurrency = pickerDataSource.first }
        currencyTextField.text = selectedCurrency
        currencyTextField.resignFirstResponder()
        CurrencyPickerView.getExchangeRate(currency: selectedCurrency ?? "INR", completion: { [weak self] rates, error in
            guard error == nil else {
                return
            }
            DispatchQueue.main.async {
                if let country = rates?.country, !country.isEmpty {
                    self?.countryNameLabel.isHidden = false
                    self?.countryNameLabel.text = country
                }
                
                if let currency = rates?.currencyName, !currency.isEmpty {
                    self?.currencyNameLabel.isHidden = false
                    self?.currencyNameLabel.text = currency
                }
                
                if let buyRate = rates?.buyTT, !buyRate.isEmpty {
                    self?.buyPriceLabel.isHidden = false
                    self?.buyPriceLabel.text = "Buy: \(buyRate)"
                }
                
                if let sellRate = rates?.sellTT, !sellRate.isEmpty {
                    self?.sellPriceLabel.isHidden = false
                    self?.sellPriceLabel.text = "Sell: \(sellRate)"
                }
                
                if let lastUpdated = rates?.lastupdated,
                   !lastUpdated.isEmpty,
                   let date = lastUpdated.getFormattedDate(inputFormat: "hh:mm a dd MMM yyyy",
                                                           outputFormat: "dd MMM yyyy hh:mm a") {
                    self?.lastUpdatedLabel.isHidden = false
                    self?.lastUpdatedLabel.text = "Last Updated: \(date)"
                }
            }
        })
    }
}

// MARK: Fetching currency rates and passing to app to be consumed according to app requirements
extension CurrencyPickerView {
    public static func getExchangeRate(currency: String?, completion: @escaping (Rate?, Error?) -> ()) {
        ExchangeRateService().getExchangeRate(handler: { result in
            switch result {
            case .failure(let error):
                completion(nil, error)
            case .success(let response):
                if let value = response.filter({ $0.value.productID == currency}).first?.value.rates.first?.value as? Rate {
                    completion(value, nil)
                }
            }
        })
    }
    
    
    public static func getExchangeRate(completion: @escaping ([String: Product]?, Error?) -> ()) {
        ExchangeRateService().getExchangeRate(handler: { result in
            switch result {
            case .failure(let error):
                completion(nil, error)
            case .success(let response):
                completion(response, nil)
            }
        })
    }
}

// MARK: Update Label Properties and Textfield Height
extension CurrencyPickerView {
    public func updateTextFieldHeight(_ height: CGFloat) {
        textFieldHeightConstraint?.constant = height
    }
    
    public var textFieldFont: UIFont {
        get {
            return currencyTextField.font ?? .systemFont(ofSize: 14.0)
        }
        set {
            currencyTextField.font = newValue
        }
    }
    
    public var textFieldColor: UIColor {
        get {
            return currencyTextField.textColor ?? .black
        }
        set {
            currencyTextField.textColor = newValue
        }
    }
    
    public var textFieldBorderColor: CGColor {
        get {
            return currencyTextField.layer.borderColor ?? UIColor.black.cgColor
        }
        set {
            currencyTextField.layer.borderColor = newValue
        }
    }
    
    public var textFieldBorderWidth: CGFloat {
        get {
            return currencyTextField.layer.borderWidth
        }
        set {
            currencyTextField.layer.borderWidth = newValue
        }
    }
    
    public var titleText: String {
        get {
            return titleLabel.text ?? ""
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    public var titleFont: UIFont {
        get {
            return titleLabel.font ?? .systemFont(ofSize: 14.0)
        }
        set {
            titleLabel.font = newValue
        }
    }
    
    public var titleColor: UIColor {
        get {
            return titleLabel.textColor ?? .black
        }
        set {
            titleLabel.textColor = newValue
        }
    }
    
    public var currencyText: String {
        get {
            return currencyNameLabel.text ?? ""
        }
        set {
            currencyNameLabel.text = newValue
        }
    }
    
    public var currencyFont: UIFont {
        get {
            return currencyNameLabel.font ?? .systemFont(ofSize: 14.0)
        }
        set {
            currencyNameLabel.font = newValue
        }
    }
    
    public var currencyColor: UIColor {
        get {
            return currencyNameLabel.textColor ?? .black
        }
        set {
            currencyNameLabel.textColor = newValue
        }
    }
    
    public var countryText: String {
        get {
            return countryNameLabel.text ?? ""
        }
        set {
            countryNameLabel.text = newValue
        }
    }
    
    public var countryFont: UIFont {
        get {
            return countryNameLabel.font ?? .systemFont(ofSize: 14.0)
        }
        set {
            countryNameLabel.font = newValue
        }
    }
    
    public var countryColor: UIColor {
        get {
            return countryNameLabel.textColor ?? .black
        }
        set {
            countryNameLabel.textColor = newValue
        }
    }
    
    
    public var buyText: String {
        get {
            return buyPriceLabel.text ?? ""
        }
        set {
            buyPriceLabel.text = newValue
        }
    }
    
    public var buyFont: UIFont {
        get {
            return buyPriceLabel.font ?? .systemFont(ofSize: 14.0)
        }
        set {
            buyPriceLabel.font = newValue
        }
    }
    
    public var buyColor: UIColor {
        get {
            return buyPriceLabel.textColor ?? .black
        }
        set {
            buyPriceLabel.textColor = newValue
        }
    }
    
    public var sellText: String {
        get {
            return sellPriceLabel.text ?? ""
        }
        set {
            sellPriceLabel.text = newValue
        }
    }
    
    public var sellFont: UIFont {
        get {
            return sellPriceLabel.font ?? .systemFont(ofSize: 14.0)
        }
        set {
            sellPriceLabel.font = newValue
        }
    }
    
    public var sellColor: UIColor {
        get {
            return sellPriceLabel.textColor ?? .black
        }
        set {
            sellPriceLabel.textColor = newValue
        }
    }
    
    public var lastUpdatedText: String {
        get {
            return lastUpdatedLabel.text ?? ""
        }
        set {
            lastUpdatedLabel.text = newValue
        }
    }
    
    public var lastUpdatedFont: UIFont {
        get {
            return lastUpdatedLabel.font ?? .systemFont(ofSize: 14.0)
        }
        set {
            lastUpdatedLabel.font = newValue
        }
    }
    
    public var lastUpdatedColor: UIColor {
        get {
            return lastUpdatedLabel.textColor ?? .black
        }
        set {
            lastUpdatedLabel.textColor = newValue
        }
    }
}

extension CurrencyPickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }

    public func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }

    public func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCurrency = pickerDataSource[row]
    }
}
