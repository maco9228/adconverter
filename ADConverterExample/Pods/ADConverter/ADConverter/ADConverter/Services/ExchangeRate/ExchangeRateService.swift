import Foundation

protocol ExchangeServiceProtocol {
    typealias Handler = (Result<[String: Product], Error>) -> Void
    func getExchangeRate(handler: @escaping Handler)
}

class ExchangeRateService: ExchangeServiceProtocol {
    
    func getExchangeRate(handler: @escaping Handler) {
        guard let url = URL(string: ServiceResources.exchangeUri) else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let anError = error {
                handler(.failure(anError))
            }else{
                if let res = data {
                    do {
                        let rate = try JSONDecoder().decode(ExchangeRateModel.self, from: res)
                        handler(.success(rate.data.brands.wbc.portfolios.fx.products))
                    } catch {
                        handler(.failure(error))
                    }
                }
            }
        }
        task.resume()
    }
}
