//
//  ExchangeRateServiceTests.swift
//  ADConverterTests
//
//  Created by manu.a.gupta on 23/03/22.
//

import XCTest
@testable import ADConverter

class ExchangeRateServiceTests: XCTestCase {
    
    var sut: ExchangeRateService?
    
    override func setUp() {
        super.setUp()
        sut = ExchangeRateService()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_getExchangeRate_success() {
        let sut = self.sut!
        let expect = XCTestExpectation(description: "callback")
        
        sut.getExchangeRate { result in
            expect.fulfill()
            switch result {
            case .success(let response):
                XCTAssertEqual(response.data.brands.wbc.portfolios.fx.products.count > 0, true)
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription.isEmpty, false)
            }
            XCTAssertNotNil(result)
        }
        
        wait(for: [expect], timeout: 5)
    }
}
