//
//  ADConverterTests.swift
//  ADConverterTests
//
//  Created by manu.a.gupta on 23/03/22.
//

import XCTest
@testable import ADConverter

class CurrencyPickerViewModelTests: XCTestCase {

    var sut: CurrencyPickerViewModel!
    var mockExchangeService: MockExchangeService!
    
    override func setUp() {
        super.setUp()
        mockExchangeService = MockExchangeService()
        sut = CurrencyPickerViewModel(exchangeRateService: mockExchangeService)
    }
    
    override func tearDown() {
        sut = nil
        mockExchangeService = nil
        super.tearDown()
    }
    
    func test_getExchangeRate_Success() {
        sut.getExchangeRate() { [weak self] rate, error in
            XCTAssert(self?.mockExchangeService!.isGetExchangeRate ?? false)
        }
        self.mockExchangeService.fetchSuccess()
    }
    
    func test_getExchangeRate_Currency_Success() {
        sut.getExchangeRate(currency: "INR") { [weak self] rate, error in
            XCTAssert(self?.mockExchangeService!.isGetExchangeRate ?? false)
        }
        
        self.mockExchangeService.fetchSuccess()
    }
    
    func test_getExchangeRate_Currency_Fail() {
        sut.getExchangeRate(currency: "INR") { [weak self] rate, error in
            XCTAssert(self?.mockExchangeService!.isGetExchangeRate ?? false)
        }
        
        self.mockExchangeService.fetchFail(.noData)
    }
    
    func test_getExchangeRate_Fail() {
        sut.getExchangeRate() { [weak self] rate, error in
            XCTAssert(self?.mockExchangeService!.isGetExchangeRate ?? false)
        }
        
        let error = NetworkError.serverError(statusCode: 403)
        self.mockExchangeService.fetchFail(.transportError(error))
    }
}

class MockExchangeService: ExchangeServiceProtocol {
    var isGetExchangeRate = false
    
    var completePhotos: ExchangeRate = StubGenerator().stubExchangeRate()
    var completeClosure: ((Result<ExchangeRate, NetworkError>) -> Void)!
    
    func getExchangeRate(completion: @escaping Completion) {
        isGetExchangeRate = true
        completeClosure = completion
    }
    
    func fetchSuccess() {
        completeClosure(.success(completePhotos))
    }
    
    func fetchFail(_ error: NetworkError) {
        completeClosure(.failure(error))
    }
}

class StubGenerator {
    func stubExchangeRate() -> ExchangeRate {
        let path = Bundle(for: CurrencyPickerViewModelTests.self).url(forResource: "ExchangeRateResponse", withExtension: "json")!
        let data = try! Data(contentsOf: path)
        let decoder = JSONDecoder()
        let exchangeRate = try! decoder.decode(ExchangeRate.self, from: data)
        return exchangeRate
    }
}
