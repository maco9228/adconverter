
import Foundation

struct CurrencyPickerViewModel {
    
    let exchangeRateService: ExchangeServiceProtocol

    init(exchangeRateService: ExchangeServiceProtocol = ExchangeRateService()) {
        self.exchangeRateService = exchangeRateService
    }
    
    func getExchangeRate(currency: String?, completion: @escaping (Rate?, String?) -> ()) {
        exchangeRateService.getExchangeRate(completion: { result in
            switch result {
            case .failure(let error):
                completion(nil, handleErrorCases(error))
            case .success(let response):
                let products = response.data.brands.wbc.portfolios.fx.products
                if let value = products.filter({ $0.value.productID == currency}).first?.value.rates.first?.value as? Rate {
                    completion(value, nil)
                }
            }
        })
    }
    
    
    func getExchangeRate(completion: @escaping ([String: Product]?, String?) -> ()) {
        exchangeRateService.getExchangeRate(completion: { result in
            switch result {
            case .failure(let error):
                completion(nil, handleErrorCases(error))
            case .success(let response):
                let products = response.data.brands.wbc.portfolios.fx.products
                completion(products, nil)
            }
        })
    }
    
    func handleErrorCases(_ error: NetworkError) -> String {
        switch error {
        case .decodingError(let error):
            return error.localizedDescription
        case .encodingError(let error):
            return error.localizedDescription
        case .noData:
            return error.localizedDescription
        case .transportError(let error):
            return error.localizedDescription
        case .serverError(let statusCode):
            print(statusCode)
            return error.localizedDescription
        }
    }
}
