import Foundation

protocol ExchangeServiceProtocol: AnyObject {
    typealias Completion = (Result<ExchangeRate, NetworkError>) -> Void
    func getExchangeRate(completion: @escaping Completion)
}

class ExchangeRateService: ExchangeServiceProtocol {
    
    private let exchangeRateDecoder = ResultDecoder<ExchangeRate> { data in
        try JSONDecoder().decode(ExchangeRate.self, from: data)
    }
    
    func getExchangeRate(completion: @escaping Completion) {
        guard let url = URL(string: ServiceResources.exchangeUri) else { return }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) { result in
            completion(self.exchangeRateDecoder.decode(result))
        }.resume()
    }
}
