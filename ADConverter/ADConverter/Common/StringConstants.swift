//
//  Strings.swift
//  ADConverter
//
//  Created by manu.a.gupta on 20/03/22.
//

import Foundation

struct StringConstants {
    static let titleText = "Please select the Currency from picker to current conversion rate to Australian Dollar."
    static let cancelButton = "Cancel"
    static let doneButton = "Done"
    static let timeZoneInputFormat = "yyyyMMdd'T'hhmmss,SSZZZZ"
    static let timeZoneOutputFormat = "dd MMM yyyy hh:mm:ss.SSZ"
    static let withoutTimeZoneInputFormat = "hh:mm a dd MMM yyyy"
    static let withoutTimeZoneOutputFormat = "hh:mm a dd MMM yyyy"
    static let currencyList = ["BDT", "WST", "TOP", "CAD", "JPY", "TWD", "THB", "KRW", "SBD", "INR", "ZAR", "XPF", "PHP", "CNH", "BRL", "SGD", "HKD", "NOK", "USD", "MYR", "AED", "VND", "EUR", "ARS", "BND", "SEK", "PGK", "NZD", "SAR", "CLP", "FJD", "CNY", "VUV", "XAU", "PKR", "GBP", "IDR", "CHF", "DKK", "LKR"]
    static let defaultCurrency = "INR"
    static let buy = "Buy"
    static let sell = "Sell"
    static let lastUpdated = "Last Updated"
    static let effectiveDate = "Effective Date"
    static let updatedFMT = "Updated FMT"
}
