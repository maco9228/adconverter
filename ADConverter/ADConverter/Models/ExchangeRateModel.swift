import Foundation

// MARK: - ExchangeRateModel
struct ExchangeRate: Codable {
    let apiVersion: String
    let status: Int
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let brands: Brands

    enum CodingKeys: String, CodingKey {
        case brands = "Brands"
    }
}

// MARK: - Brands
struct Brands: Codable {
    let wbc: Wbc

    enum CodingKeys: String, CodingKey {
        case wbc = "WBC"
    }
}

// MARK: - Wbc
struct Wbc: Codable {
    let brand: String
    let portfolios: Portfolios

    enum CodingKeys: String, CodingKey {
        case brand = "Brand"
        case portfolios = "Portfolios"
    }
}

// MARK: - Portfolios
struct Portfolios: Codable {
    let fx: Fx

    enum CodingKeys: String, CodingKey {
        case fx = "FX"
    }
}

// MARK: - Fx
struct Fx: Codable {
    let portfolioID: String
    let products: [String: Product]

    enum CodingKeys: String, CodingKey {
        case portfolioID = "PortfolioId"
        case products = "Products"
    }
}

// MARK: - Product
public struct Product: Codable {
    let productID: String
    let rates: [String: Rate]

    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case rates = "Rates"
    }
}

// MARK: - Rate
public struct Rate: Codable {
    let currencyCode, currencyName, country, buyTT: String
    let sellTT, buyTC, buyNotes, sellNotes: String
    let spotRateDateFmt, effectiveDateFmt, updateDateFmt, lastupdated: String

    enum CodingKeys: String, CodingKey {
        case currencyCode, currencyName, country, buyTT, sellTT, buyTC, buyNotes, sellNotes
        case spotRateDateFmt = "SpotRate_Date_Fmt"
        case effectiveDateFmt = "effectiveDate_Fmt"
        case updateDateFmt = "updateDate_Fmt"
        case lastupdated = "LASTUPDATED"
    }
}
