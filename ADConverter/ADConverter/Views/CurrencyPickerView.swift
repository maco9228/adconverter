//
//  CurrencyPickerView.swift
//  ADConverter
//
//  Created by manu.a.gupta on 20/03/22.
//

import UIKit

public class CurrencyPickerView: UIView {
    
    lazy private var contentView: UIView = makeContentView()
    lazy private var stackView: UIStackView = makeVerticalStackView()
    lazy private var titleLabel: UILabel = makeTitleLabel()
    lazy private var currencyTextField: UITextField = makeCurrencyTextField()
    lazy private var currencyDetailStackView: UIStackView = makeCurrencyStackView()
    lazy private var countryNameLabel: UILabel = makeCountryNameLabel()
    lazy private var currencyNameLabel: UILabel = makeCurrencyNameLabel()
    lazy private var rateDetailStackView: UIStackView = makeRateStackView()
    lazy private var buyPriceLabel: UILabel = makeBuyPriceLabel()
    lazy private var sellPriceLabel: UILabel = makeSellPriceLabel()
    lazy private var lastUpdatedLabel: UILabel = makeLastUpdatedLabel()
    lazy private var effectiveDateLabel: UILabel = makeEffectiveDateLabel()
    lazy private var updateDateFMTLabel: UILabel = makeUpdateDateFmtLabel()
    
    private let pickerView = UIPickerView()
    private var pickerDataSource = StringConstants.currencyList.sorted()
    private var selectedCurrency: String?
    private var textFieldHeightConstraint: NSLayoutConstraint?
    lazy private var viewModel: CurrencyPickerViewModel = {
        return CurrencyPickerViewModel()
    }()
     
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        setupUI()
    }
    
    private func setupUI() {
        addSubview(contentView)
        contentView.bindFrameToSuperviewBounds()
        
        stackView.addArrangedSubview(titleLabel)
        
        textFieldHeightConstraint = currencyTextField.heightAnchor.constraint(equalToConstant: Constants.TextField.height)
        textFieldHeightConstraint?.isActive = true
        
        stackView.addArrangedSubview(currencyTextField)
        contentView.addSubview(stackView)
        
        countryNameLabel.isHidden = true
        currencyNameLabel.isHidden = true
        
        currencyDetailStackView.addArrangedSubview(countryNameLabel)
        currencyDetailStackView.addArrangedSubview(currencyNameLabel)
        stackView.addArrangedSubview(currencyDetailStackView)
        
        buyPriceLabel.isHidden = true
        sellPriceLabel.isHidden = true
        
        rateDetailStackView.addArrangedSubview(buyPriceLabel)
        rateDetailStackView.addArrangedSubview(sellPriceLabel)
        stackView.addArrangedSubview(rateDetailStackView)
        
        lastUpdatedLabel.isHidden = true
        stackView.addArrangedSubview(lastUpdatedLabel)
        
        
        effectiveDateLabel.isHidden = true
        stackView.addArrangedSubview(effectiveDateLabel)
        
        updateDateFMTLabel.isHidden = true
        stackView.addArrangedSubview(updateDateFMTLabel)

        stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.Padding.left).isActive = true
        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.Padding.left).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.Padding.left).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constants.Padding.left).isActive = true
        
        pickerView.delegate = self
        currencyTextField.inputView = pickerView
    }
    
    @objc private func tapDone() {
        if selectedCurrency == nil { selectedCurrency = pickerDataSource.first }
        currencyTextField.text = selectedCurrency
        currencyTextField.resignFirstResponder()
        viewModel.getExchangeRate(currency: selectedCurrency ?? StringConstants.defaultCurrency, completion: { [weak self] rates, errorString in
            guard errorString == nil else {
                self?.countryNameLabel.isHidden = false
                self?.countryNameLabel.text = errorString
                return
            }
            DispatchQueue.main.async {
                if let country = rates?.country, !country.isEmpty {
                    self?.countryNameLabel.isHidden = false
                    self?.countryNameLabel.text = country
                }
                
                if let currency = rates?.currencyName, !currency.isEmpty {
                    self?.currencyNameLabel.isHidden = false
                    self?.currencyNameLabel.text = currency
                }
                
                if let buyRate = rates?.buyTT, !buyRate.isEmpty {
                    self?.buyPriceLabel.isHidden = false
                    self?.buyPriceLabel.text = "\(StringConstants.buy): \(buyRate)"
                }
                
                if let sellRate = rates?.sellTT, !sellRate.isEmpty {
                    self?.sellPriceLabel.isHidden = false
                    self?.sellPriceLabel.text = "\(StringConstants.sell): \(sellRate)"
                }
                
                if let lastUpdated = rates?.lastupdated,
                   !lastUpdated.isEmpty,
                   let date = lastUpdated.getFormattedDate(inputFormat: StringConstants.withoutTimeZoneInputFormat,
                                                           outputFormat: StringConstants.withoutTimeZoneOutputFormat) {
                    self?.lastUpdatedLabel.isHidden = false
                    self?.lastUpdatedLabel.text = "\(StringConstants.lastUpdated): \(date)"
                }
                
                if let effectiveDate = rates?.effectiveDateFmt,
                   !effectiveDate.isEmpty,
                   let date = effectiveDate.getFormattedDate(inputFormat: StringConstants.timeZoneInputFormat,
                                                             outputFormat: StringConstants.timeZoneOutputFormat) {
                    self?.effectiveDateLabel.isHidden = false
                    self?.effectiveDateLabel.text = "\(StringConstants.effectiveDate): \(date)"
                }
                
                if let updateDateFmt = rates?.updateDateFmt,
                   !updateDateFmt.isEmpty,
                   let date = updateDateFmt.getFormattedDate(inputFormat: StringConstants.timeZoneInputFormat,
                                                           outputFormat: StringConstants.timeZoneOutputFormat) {
                    self?.updateDateFMTLabel.isHidden = false
                    self?.updateDateFMTLabel.text = "\(StringConstants.updatedFMT): \(date)"
                }
            }
        })
    }
    
    // MARK: Fetching currency rates and passing to app to be consumed according to app requirements
    public func getExchangeRate(currency: String?, completion: @escaping (Rate?, String?) -> ()) {
        viewModel.getExchangeRate(currency: currency, completion: completion)
    }
    
    public func getExchangeRate(completion: @escaping ([String: Product]?, String?) -> ()) {
        viewModel.getExchangeRate(completion: completion)
    }
}

// MARK: UIElements
extension CurrencyPickerView {
    private func makeContentView() -> UIView {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeVerticalStackView() -> UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = .vertical
        stackView.spacing = Constants.Stack.spacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    private func makeTitleLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.Fonts.titleFont)
        label.textColor = .black
        label.text = StringConstants.titleText
        return label
    }
    
    private func makeCurrencyTextField() -> UITextField {
        let textField = UITextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.setInputToobar(target: self, selector: #selector(tapDone))
        textField.layer.borderColor = UIColor.black.cgColor
        textField.layer.borderWidth = 1
        textField.setLeftPaddingPoints(Constants.Padding.labelLeft)
        return textField
    }
    
    private func makeCurrencyStackView() -> UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .leading
        stackView.spacing = Constants.Stack.spacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    private func makeCountryNameLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.Fonts.subTitleFont)
        label.textColor = .black
        return label
    }
    
    private func makeCurrencyNameLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.Fonts.subTitleFont)
        label.textColor = .black
        return label
    }
    
    private func makeRateStackView() -> UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = .horizontal
        stackView.alignment = .leading
        stackView.distribution = .equalSpacing
        stackView.spacing = Constants.Stack.spacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    private func makeBuyPriceLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.Fonts.subTitleFont)
        label.textColor = .black
        return label
    }
    
    private func makeSellPriceLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.Fonts.subTitleFont)
        label.textColor = .black
        return label
    }
    
    private func makeLastUpdatedLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.Fonts.subTitleFont)
        label.textColor = .black
        return label
    }
    
    private func makeEffectiveDateLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.Fonts.subTitleFont)
        label.textColor = .black
        return label
    }
    
    private func makeUpdateDateFmtLabel() -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.Fonts.subTitleFont)
        label.textColor = .black
        return label
    }
}

// MARK: Update Label Properties and Textfield Height
extension CurrencyPickerView {
    public func updateTextFieldHeight(_ height: CGFloat) {
        textFieldHeightConstraint?.constant = height
    }
    
    public var textFieldFont: UIFont {
        get {
            return currencyTextField.font ?? .systemFont(ofSize: Constants.Fonts.titleFont)
        }
        set {
            currencyTextField.font = newValue
        }
    }
    
    public var textFieldColor: UIColor {
        get {
            return currencyTextField.textColor ?? .black
        }
        set {
            currencyTextField.textColor = newValue
        }
    }
    
    public var textFieldBorderColor: CGColor {
        get {
            return currencyTextField.layer.borderColor ?? UIColor.black.cgColor
        }
        set {
            currencyTextField.layer.borderColor = newValue
        }
    }
    
    public var textFieldBorderWidth: CGFloat {
        get {
            return currencyTextField.layer.borderWidth
        }
        set {
            currencyTextField.layer.borderWidth = newValue
        }
    }
    
    public var titleText: String {
        get {
            return titleLabel.text ?? ""
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    public var titleFont: UIFont {
        get {
            return titleLabel.font ?? .systemFont(ofSize: Constants.Fonts.subTitleFont)
        }
        set {
            titleLabel.font = newValue
        }
    }
    
    public var titleColor: UIColor {
        get {
            return titleLabel.textColor ?? .black
        }
        set {
            titleLabel.textColor = newValue
        }
    }
    
    public var currencyFont: UIFont {
        get {
            return currencyNameLabel.font ?? .systemFont(ofSize: Constants.Fonts.subTitleFont)
        }
        set {
            currencyNameLabel.font = newValue
        }
    }
    
    public var currencyColor: UIColor {
        get {
            return currencyNameLabel.textColor ?? .black
        }
        set {
            currencyNameLabel.textColor = newValue
        }
    }
    
    public var countryFont: UIFont {
        get {
            return countryNameLabel.font ?? .systemFont(ofSize: Constants.Fonts.subTitleFont)
        }
        set {
            countryNameLabel.font = newValue
        }
    }
    
    public var countryColor: UIColor {
        get {
            return countryNameLabel.textColor ?? .black
        }
        set {
            countryNameLabel.textColor = newValue
        }
    }
    
    public var buyFont: UIFont {
        get {
            return buyPriceLabel.font ?? .systemFont(ofSize: Constants.Fonts.subTitleFont)
        }
        set {
            buyPriceLabel.font = newValue
        }
    }
    
    public var buyColor: UIColor {
        get {
            return buyPriceLabel.textColor ?? .black
        }
        set {
            buyPriceLabel.textColor = newValue
        }
    }
    
    public var sellFont: UIFont {
        get {
            return sellPriceLabel.font ?? .systemFont(ofSize: Constants.Fonts.subTitleFont)
        }
        set {
            sellPriceLabel.font = newValue
        }
    }
    
    public var sellColor: UIColor {
        get {
            return sellPriceLabel.textColor ?? .black
        }
        set {
            sellPriceLabel.textColor = newValue
        }
    }
    
    public var lastUpdatedFont: UIFont {
        get {
            return lastUpdatedLabel.font ?? .systemFont(ofSize: Constants.Fonts.subTitleFont)
        }
        set {
            lastUpdatedLabel.font = newValue
        }
    }
    
    public var lastUpdatedColor: UIColor {
        get {
            return lastUpdatedLabel.textColor ?? .black
        }
        set {
            lastUpdatedLabel.textColor = newValue
        }
    }
}

extension CurrencyPickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }

    public func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }

    public func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCurrency = pickerDataSource[row]
    }
}

extension CurrencyPickerView {
    struct Constants {
        struct Fonts {
            static let titleFont = 14.0
            static let subTitleFont = 12.0
        }
        struct Padding {
            static let left = 24.0
            static let labelLeft = 12.0
        }
        struct Stack {
            static let spacing = 12.0
        }
        struct TextField {
            static let height = 50.0
        }
    }
}
