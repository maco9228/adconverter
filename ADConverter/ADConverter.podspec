Pod::Spec.new do |spec|

  spec.name         = "ADConverter"
  spec.version      = "1.0.1"
  spec.summary      = "ADConverter is currency converter used for get currency rates for more then 30 currency aganst the Australian Dollar "
  spec.homepage     = "https://gitlab.com/maco9228/adconverter"
  spec.license      = {:type => "MIT", :file => "../LICENSE"}
  spec.author       = { "Manu Gupta" => "manugupta9228@gmail.com" }
  spec.platform     = :ios, "9.0"
  spec.source       = { :git => "https://maco9228@gitlab.com/maco9228/adconverter.git", :tag => "#{spec.version}" }
  spec.source_files  = "ADConverter/**/*.{swift}"
  spec.framework  = "UIKit", "Foundation"
  spec.requires_arc = true
  spec.swift_version = "5.0"
  
end
